@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <br>
                <br>

                <h3> Tabla de visualización </h3>
                <br>
                <br>
                <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Nueva Persona </a>
            </div>
        </div>
    </div>
	
    @if ($message = Session::get('success'))
	<br><br>
        <div class="alert alert-success">
            <p> {{ $message }} </p>
        </div>
    @endif
    <br><br>
    <table class="table table-bordered">
        <tr>
            <th> No </th>
            <th> NOMBRE </th>
            <th> DOCUMENTO </th>
            <th> GENERO </th>
            <th> EDAD </th>
            <th> TELEFONO </th>
            <th> EPS </th>
            <th> ROL </th>
            <th> OPCIONES </th>

        </tr>
        @foreach ($products as $key => $product)
            <tr>
                @php
                    //$fechanacimiento =$product->fecha_nacimiento;
                    $validar = true;
                    $fechanacimiento = $product->fecha_nacimiento;
                    [$ano, $mes, $dia] = explode('-', $fechanacimiento);
                    $ano_diferencia = date('Y') - $ano;
                    $mes_diferencia = date('m') - $mes;
                    $dia_diferencia = date('d') - $dia;
                    if ($dia_diferencia < 0 || $mes_diferencia < 0) {
                        $ano_diferencia--;
                    }
                    
                    $product->fecha_nacimiento = $ano_diferencia;
                    if ($ano_diferencia > 50) {
                        $validar = true;
                    } else {
                        $validar = false;
                    }
                    
                @endphp
                @if ($ano_diferencia > 50)
                    <td bgcolor="red"> {{ ++$i }} </td>
                    <td bgcolor="red"> {{ $product->nombre }} </td>
                    <td bgcolor="red"> {{ $product->documento }} </td>
                    <td bgcolor="red"> {{ $product->genero }} </td>
                    <td bgcolor="red">
                        {{ $product->fecha_nacimiento }}
                    </td>
                    <td bgcolor="red"> {{ $product->telefono }} </td>
                    <td bgcolor="red"> {{ "Sura" }} </td>
                    <td bgcolor="red"> {{ "usuario" }} </td>
                    <td bgcolor="red">
                        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                            <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                @endif
                @if ($ano_diferencia < 50)

                    <td bgcolor="green"> {{ ++$i }} </td>
                    <td bgcolor="green"> {{ $product->nombre }} </td>
                    <td bgcolor="green"> {{ $product->documento }} </td>
                    <td bgcolor="green"> {{ $product->genero }} </td>
                    <td bgcolor="green">
                        {{ $product->fecha_nacimiento }}
                    </td>
                    <td bgcolor="green"> {{ $product->telefono }} </td>
                    <td bgcolor="green"> {{ "nueva eps" }} </td>
                    <td bgcolor="green"> {{ "usuario" }} </td>
                    <td bgcolor="green">
                        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                            <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
            </tr>
        @endif
        @endforeach
    </table>
@endsection
