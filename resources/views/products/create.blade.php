@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h3> Añadir nueva persona </h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.index') }}"> Regresar a vista visualizacion </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oopps! </strong> Something went wrong.
            <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="nombre" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Documento:</strong>
                    <input type="text" name="documento" class="form-control" placeholder="Documento">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Password:</strong>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Genero:</strong>
                    <input type="text" name="genero" class="form-control" placeholder="Genero">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Telefono:</strong>
                    <input type="text" name="telefono" class="form-control" placeholder="Telefono">
                </div>
            </div>
            <div class="col-lg-12">
                <strong>Fecha Nacimiento:</strong>
                <div class="col-10">
                    <input class="form-control" name="fecha_nacimiento" type="date" value="2011-08-19"
                        id="example-date-input">
                </div>
            </div>
			

            <div class="col-lg-12">
				<br><br>

                <button type="submit" class="btn btn-primary">Crear Persona</button>
            </div>
        </div>
    </form>
@endsection
