@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h3> Editar persona </h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.index') }}"> Regresar a vista visualizacion </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oopps! </strong> Something went wrong.
            <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.update', $product->id) }}" method="POST">
        @csrf
        @method("PUT")
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="nombre" class="form-control" value="{{ $product->nombre }}">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Documento:</strong>
                    <input type="text" name="documento" class="form-control" value="{{ $product->documento }}">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Password:</strong>
                    <input type="password" name="password" class="form-control" value="{{ $product->password }}">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Genero:</strong>
                    <input type="text" name="genero" class="form-control" value="{{ $product->genero }}">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <strong>Telefono:</strong>
                    <input type="text" name="telefono" class="form-control" value="{{ $product->telefono }}">
                </div>
            </div>
            <div class="col-lg-12">
                <strong>Fecha Nacimiento:</strong>
                <div class="col-10">
                    <input class="form-control" name="fecha_nacimiento" type="date" value="{{ $product->fecha_nacimiento }}"
                        id="example-date-input">
                </div>
            </div>


            <div class="col-lg-12">
                <br><br>

                <button type="submit" class="btn btn-primary">Editar Persona</button>
            </div>
        </div>
    </form>
@endsection
